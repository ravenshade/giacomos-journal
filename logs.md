# Find Yourself in Yourself [10/14/2021]
 - Return the gem to spider
 - Map of cruiser (with cargo) from Sinright city to Northern Iklemore ("food and water")
 - Northern Iklemore would be a walk to a boat that goes to Demara
 - Spider keeps an open hand for 'status and work' if we ever need
 - Esmeralda's Swords, 
 - Black Powder weapons: 
 pistol: 125g 2d4 pierce, 
 rifle : 300g 2d6 pierce,
 sniper: 450g 2d10 pierce,
 ammo  : 3s per bullet
 - buy two pistols, ammo, and head to the ship
### This Ship Sucks
 - Ship is crampt, we get a tiny room, mostly full of cargo
 - Storm appears after a day voyage, Jack and I are inspecting cargo when one 
 comes alive into a clockwork spider
 - We take it down, but it exploded a 30,000g car
 - Piotr Andrez Clockwork Retriever make and model
 - Pilot comes down, tries to implicate us, we capture him and the crew. Giacomo
 shoots off his lower body.
 - Party discusses what approach we'll take. Decide to sending the Spider,
 spider says to dock with the ship and that it is unfortunate.
 - Giacomo opposes, arguing we should crash and burn ship, no one else agrees.
 - Magnet ensists that the Spider intends to kill us
### The Good Graces of the Spider
 - Jack and Magnet hop off in order to scout ahead
 - The rest of the party hides. 
 - The two find *nobody* and just dockworkers.
### "Up North" City
 - Arrive at city, take subway across town (8g)
 - Ride motorcycles over towards the ferries, without motors.
 - Buried the Bag of Holding with all the stuff in a forest and marked it
### Stranger in a strange land
 - People look odd -- wear robes, use canes, almond eyes, sandals (we're in asia)
 - Giacomo buys a map of Iklemore
 - Cross into Demara
 - Area is feudal, low technology level, full of fishing supplies and clothes
 - Residents are speaking Demaran
 - Clinc, Mer, and Evan all purchase nice kimonos to fit in better
 - Find a Samurai named Mushi who tells us "this is a land that you should not
 bring violence to."
 - Hire translator named Yu for 1 silver per day. "We let the land guide us here"
 - We want to become an acolyte of the Yellow Rose
 "Many come searching for the yellow rose, but yellow rose has to choose YOU"
 - Jack tries to attune to nature, hears the buzzing of a bee, Jack gains ki point
### Unlock all the chakras
 - Evan goes over to a cherry blossom tree area, a nice bamboo garden
 - Evan disappariates into .... his own... chakras, or something
 - Jack realizes that Evan reached self enlightment
 - Yu continues trying to explain spiritual enlightment, amazed that Evan got it
 in 20 minutes
 - Yu forces Giacomo into a sauna, he passes out
 - Giacomo has a mental breakdown, tries running into a tree, gets hurt
 - Giacomo throws all his stuff onto the ground, yelling that he's lost everything
 The bamboo envelops him, letting him through.
 - A turtle named Yu visits us in the bamboo realm
 - Spirit Animals:
 Evan: Bear
 Jack: Fox
 Mer: Cat
 Mag: Octopus
 Clinc: Sparrow
 Giacomo: Hermit Crab

# Whiplash [10/7/2021]
 - we spent 2 months under the dark (Canon!?)
 - we arrive early at 5am at the farm stop on the train rail (8 hour train ride away)
 - Drop off Arthur and Evan to go find the family farm and meet up with us
 - Met with Foreman Kanon who directed us to a nearby village with car transportation the arnolodge
 - Luber Service (transportation)
### Road Trip
 - Stay the night for 4g each, buy two motorcycles because we can't afford luber
 - Giacomo and Jack both try to drive, Giacomo did a slightly better job
 - Giacomo drives the whole trip, ~10 hours, but Jack was exhausted by the end, mer
 is riding up front, Clinc in the bag
 - Party arrives at sinright city, a huge metal city intertwined with an 
 effiel tower made of two of them combined, overreaching the ocean
 - Casino district, "420 Casino" known for consistent highs (modest 5s / person)
 - Pay for a night's stay
### Mag-net the Magician
 - Saw a magic show where a warforged magician performed some real magic
 - Giacomo, enthralled by the performance, asked about all his tricks
 - Jack, Clinc, Mer, and Giacomo all started to devise how to acquire and free this warforged
 since he seems to have extra power
 - Mer and Clinc went gambling at the slot machines
 - Jack went carousing and found the spider gang is entrenched in everything, slave trade
 gambling, robot fighting, joining the spider gang usually just requires building a rep.
 Huge amounts of veterans live here. Higher number on spider tat means less cool. 
 Spider gang has high level of skepticism about blue lady's immortality drug
### VEGAS BABY
 - Jack plans to try to enter the spider's web by flashing some cool card tricks
 - Giacomo plans to raise money by counting cards at blackjack
 - Clinc AND Mer won a total of ~300g in slots, Jack won 91, Giacomo 163
 - Jack bet Clinc in a high stakes game to free Mag-net, we rip up his contract
 - All of us stay the night, Mag-net gets some maintenance, a bit of void slips out
 - Next day, party buys magiquartz and dapper clothing for the warforged
### Caught in the Web
 - Ran the card Jack was handed to join the gang, took elevator to the boss' room
 - Giant spider on the ceiling attached to a man, scurrying on the ceiling
 - Spider offers free transportation to a city near Demara if we retrieve something
 for him, a piece of Magiquartz that's been taken from him. Thief named Benji.
 Young man, 20s, tan skin, arcarnan (desert), capable, refused invitation. We get
 Anna, changeling, and another escort to assist us with this endeavor.
 - At bottom of the elevator, we see the Black Jackal (full black jackal armor),
 apparently our contact.
 - Agree to tentatively work with him until we finish the mission.
### Yo-yo
 - Party tracked down to the top of a skyscraper, disembarked
 - fought 2, 2, 3 agents and an acid monster as we ascend the skyscraper. At the top
 we find Benji with the magiquartz and two agents.
 - dude goes up, jack pulls down, giacomo hits and pulls him down (yoyo-ing)
 - giacomo feather falls all in air, jack tries to grapple, Mag-net sendings our ride
 - Anna heads in the skiff directly under the skyscraper to catch us falling
 - acid monster catches ben, giacomo tries to cut off hand, its metal
 - jack tries to wrestle, fails, benji tries to teleport twice and it fails
 - clinc falls from the ceiling and stunning-strikes benji in mid-air
 - Jack grabs the stone, drops onto the skiff, acid monster chases
 - Black Jackal shoots it, spewing acid over all of us, jack goes down
 - Clinc, Mer, and Jack all push the acid monster off the skiff, we sail off
### We keep the kid? Which kid? That kid..
 - Anna says that Cassidy is gone on a mission. Mag sendings her, no response.
 Mer finds her via find object over by a skyscraper, might be unconscious.
 - We keep Benji (unconcious), but they keep Cassidy and the stone, transportation
 provided as promise.
 - mer sends owl towards cassidy. Crew plans to set sail in a few days, after
 rebuilding Gizmo and getting supplies.

# Black Mesa: On a Rail [7/29/2021]
 - 3 days away from Sinright on the tram
 - Sincity known for "Black Web" criminal syndicate
 - Warforged are traded, battled, and scrapped in Sincity.
### The Tram
 - run into a door while on the tram, lever and power shut down
 - Giacomo (33 roll) begins repairing the control panel
 - Crew investigates some torn up vents and power source, hear skittering
 - Giacomo gets control panel working after 55 minutes using 15 magiquartz
 - Party is swarmed by ghouls pouring out of vents and coming for lights
 - Party fought off initial hordes, but then large ghouls and swarms appear
 - Evan goes into PTSD episode, enters a rage, doesn't hear tram start moving
 - Tram begins to move, Jack and Giacomo shoot grappling hooks at Evan
 - Jack's hit, piercing Evan, pulling him and Gizmo along with the Tram
 - Jack's arm broke, Evan held on, Gizmo fell behind and was destroyed
 - Clinc falls, but Arthur manages to save both Jack and Clinc from near death
 - Evan was able to get pulled in as the Tram escapes the hordes.
 - Giacomo repairs Clinc's arm while on the ride back
 - +1 Cassidy nat 1
### It's a long road
 - We go left at a fork (through art of deduction BTW)
 - Awkward training session with Cassidy as a fighter goes really bad
 - Jack sees an electric shock on the rail, Mer stops it with dispel magic
 - Second day passes without event.
 - Third day reaches final chamber
 - Found a slumped over black-veined assassinated agent
 - Giacomo hotwired the control panel to open up the gate
 - Party travels through the gate and into the sunlight, bright hills around us,
 seeing Machine City far in the distance

# Take me down to the Sinright City [7/8/2021]

#### Decision Tree
The Tree
Lucian Convo: Convince him to let us roam the [high seas] or [kidnap inventor] on his boat
[high seas]: he accepts high seas
[kidnap inventor]: refuses high seas, accepts inventor. 
[neither] we fight him or die or just get super sad (dead end)

Desmond Convo: After lucian, we go to talk with desmond. 
if [high seas]: Ask him to join us to go against his mother and make a real name for himself, be the Blue Prince [free boat]
OR if [high seas]: Ask him to fund a [boat-lottery] competition
if [inventor]: Ask him to help us with his mommy's spies because we in deep shit [mommy]
[free boat]: he says yes and we set sail gtfo machine city, maybe find a smaller vessel
[mommy/neither]: we try to find a boat to steal to get outta town before shit his the fan, hide lucian

leave machine city: We have to leave
if [free boat] we use it
if [boat lottery] we use it
else we try to steal a boat [grand theft boat]. If that doesn't seem doable and [kidnap inventor][neither desmond] then we [kowtow lucian]
[grand theft boat]: we manage to steal a boat
[kowtow lucian]: we attempt to kidnap the inventor and use lucian's get-away to escape

### Lu-shit the bed
 - Lucian didn't like us critizing his power versus the Blue Lady
 - Lucian summoned an ent that erupts and attacks the party, but Blue Lady portals in.
 - Blue Lady eating Lucian alive with necromancer powers
 Used Paralyzing claw. Tendrils shooting out of her hands.
 - Mer threw Gizmo off the ledge. Fire wall blocked off our escape.  
 - Blue Lady escapes with Lucian through the portal. Clockwork soldiers arrive and
 arrest Mer and Evan (Evan kept punching Mer).
 - Mer and Evan were taken to the brig by guards. Party visits after Jack comes to.

### Puttin' the Mer in Murder
 - Guard lets the others go. Desmond covered the room charges. Desmond believes that
 another assassin arrived and we protected him again. 
 - Cassidy is tracked down. She is shook. We tried to comfort.
 - Mer identified the scroll as a multidimensional portal that pulls undead from
 another realm, the sabbathune. Symbol is written in Quori. "They are malevolent
 dream spirits."
 - Mer identified the scroll written in Quori to summon a "Soulketesh" from
 another plan. Quori from Dal Quor interact with dreams in material realm. Something
 is being summoned from Dal Quor. Abilities seem to mimic Blue Lady's style.
 
### The Real Blue Lady
 - Pope & Queen. Blue Lady has been around for 120 years. Rose to power from
 noble. Machine City from Orenshire family. Blue Lady worked her way up after
 many marriages. Warforged came about last year, when the war happened. General theory
 is that she is a divine entity. 
 - Dragons are extinct. Last known is a dragon named Dyla in Terracloven. 
 - Ancient knowledge from yellow rose monk order is in Demara. 20 weeks.
 - Teal Steel Guard adds quarantine while cure is being distributed. Week until
 pill forced down throats.
 
### The Cool Kids call it Sincity
 - Considered Twilight Gap, worried about ability to navigate.
 - Travel time to Sincity (AKA Vegas BABY): Car in a few days. Quarantine is 3 months.
 Pill is taken daily for 3 months.
 - Solatium: Fuck Da Blue Lady. Commit treason for personal vendetta. Neutral Evil.
 - Jack met up with Mickey. Mickey promised transportation through solatium trams
 down below the city. Promised ammo and transportation for 7 to Sinright city. Promised
 that Demara is "the only way to take down the Blue Lady." 
 - Arthur is coming with to go with his grandparents out of hte city.
 - Got 800g, pistol, ammo, rations and headed out straight. Giacomo drove the tram.
 - Party finally leaves machine city. 

# Blue in the Face [7/1/2021]
 - Given a hotel room on Blue Diamond Power Stonecruiser (4 beds, 2 rooms, bathroom, seating, blue lady) by the Prince
 - Given tickets to the masquerade ball happening at the end of the month.
 - Arcanix can be used to develop anti-scrying amulets, some spells to counter. 
 - We go to bed after running diagnostics on Cassidy and some nightcaps.
 - In the morning, butler comes with mimosas and omelette. 
 - Have a fantastic breakfast with coffee, mimosas, swimming.
 - Beat some folks in chess in cigar lounge, Arthur plays volleyball w/ Cassidy / frats.
 - Parachute down off the stonecruiser to land in higher district (because the skiff is slow AF)
 - Sold whales for 470 gold, split it among all three. Arrive back at 11pm.
 - Arthur gets high with frat boys, smoking marijuana, alpha phi omega.
 old college buddies of Prince Desmond's. 
 - Arthur & Cassidy got a stern talking too by their "parental" figures
 - Arthur & Cassidy still leave to go to their "prior engagement" with the frat anyway **420 BLAZE IT**
 - Party hit Level 5
### Falcon on the Balcony
 - Lucian arrives. Very curious about Cassidy. 
 - Lucian requests that we sabotage the parade. Jack tries to argue why destroying the
 cure is a terrible idea. "Jack is an important piece" - master manipulator.
 - MVP Guests: Princess Azura, Prince Desmond, Vildihamen Nodur (magiquartz)
 Piotr Andrez (radios, skiffs, 'greatest inventor'),
 Kierge Lauren mythril mage (only mage in court) as masquerade ball
 - The Masquerade Ball will involve flying into tower of tears. Blue Lady will announce "the cure."
 - Piotr Andrez: relatively secluded man, sells blueprints, almost never seen. Stays in his tower.
 - Party tells Arthur about the plan for-real. Arthur insists on going to the Blue Lady first. Giacomo/Jack
 cannot agree to that plan.
 - Arthur decides to go back to the Iron Wolves. 
 
### His Name is.... Mer.
 - Mer. White long silver hair, hide armor, looks human (but isn't). Arrives with Lucian. Joins us as a spy.
 - Jack proposes the plan to kidnap the famous inventor, Piotr Andrez. Lucian agrees
 to the plan, promises exfiltration opportunities. 
 - Party creates masks (Jack fox, Mer white fox, Phantom Opera, Clinc V-Vendetta, 
 me clockwork)
 
### Old Man of the Opera
 - Bag of holding is hidden inside of Clinc (duck tape because it doesn't close all the way)
 - Arrive at the ball. Princess Azurea greeted us, told us to talk amongst ourselves.
 - Extracted bag from clinc inside the restroom, but a guard asked Clinc to leave the ship. Manage to use 'oil' excuse for why he was in the bathroom in the first place.
 - Blue Lady arrives (7 ft, blue hair), total freeze of the room as all attention shifts.
 - Time suddenly stops, but suddenly speeds up to match the rhyhthm as she seats.
 - Found inventor with two top-of-the-line clockwork soldiers, admantite mask.
 - Silver-robed spellbook-wielding approaches Evan. Old man w/ bugbear mask.
 Created ID cards using abjuration magic.
 - Jack dances with Desmond. Mer with Azura (who's undead). Evan w/ Mythril
   and Giacomo is rebuffed and thrown across the hall (literally) by Piotr.
 - Cassidy is taken by Blue Lady to dance. She was mind read (described "fingers in her mind" feeling).
### The Blue Lady
 - Dance ends, Blue Lady greets everyone. Rapunzel hair, 11/10 beauty.
 - Everyone toasts and drinks. Blue lady gives speech.
 "As you know, our city has been struck with a blight by Lucian. I went to
 a place called Demara and worked with monks called the Yellow Rose. They had
 an intricate flower that can cure any disease. I took this and modified it,
 (places yellow pill on table). This is the cure, not to just the worms, but 
 to everything. This is human-perfection in medicine form. Taking this will
 cure more than just sicknesses. This will cure death. Imagine, immortality.
 I will be distributing this to machine city over the next three months. (looks at our table) You
 all should be so lucky, I am cultivating the finest civilization in the 
 material plane. (Entire room claps). Enjoy the rest of your evening. (Hood over)"
 - Prince Desmond informed us about huge shipments of magiquartz. "I feel like I'm
 nothing (compared to her)." 
### We Need to Escape
 - Arrive with Desmond back on the Blue Diamond. Lucian was waiting for us.

# Panic! At the Disco [6/24/2021]
 - Got back on the skiff
 - Giacomo was drowning, Jack heard, party managed to bring skiff over and save him
 - Skiff took everyone back to Iron Wolves Guild
 - Icora thanked everyone for help, gave Arthur a promotion, charged him with 

### Month of down time
 - Met Roland - Vuman Bloodhunter, jaunty hat, ugly AF, nice beard, ~30-50
 - Roland goes to do some merc work, it doesn't go great
 - Arthur spends his time being a hero, restoring people
 - Clinc spends his time investigating water wheel district and healing people
### I've Got a Golden Ticket
 - Rich don't care, water is still on, 
 - all new money, blue lady is the only 'old money' court
 - Blue Lady herself is coming in the Tower of Tears, throwing party
 - Selatium wants a hit on Blue Lady, wants security detail investigated
 - Giacomo and Jack get gold card, get proficiency
 - Lucian offers to cause issues during Blue Lady visit to make us appear as heroes
 - Find an influential person's ship, can contact via the bird, get access.
### We're In
 - Get past gate. Clinc/Gizmo in bag. Keep weapons in the bag.
 - Given Diamond Card (and drugs) - Prince Desmend (Blue Lady son) on the Blue Diamond ship.
 - Reach party yacht, lots of magiquartz and shop talk
 - Crystallized Drakeroot dealt from 15 year old to Arthur and Roland
 - Get onto a skiff flashing a diamond card
### Prince Desmond Party Stonecruiser (younger brother of Blue Lady)
 - Get more drugs from some college kids
 - Desmond lined with blue magiqaurtz suit approaches us. Dark black hair. Pale.
 - Desmond talked with us - Clinc did robot, Giacomo got out gizmo, we schmoozed
 - Desmond mentioned the Blue Lady is 100 years old, but looks 20.
 - Giacomo and Roland pilfer some hot merchandise
### Taking Shots at the Party
 - "Black Jackal" bounty hunter (famous) shoots Jack in the shoulder from hoverboard
 - Jack reels, cries for help, Cassidy hides. Party arrives, gets slept.
 - Black Jackal gets his hoverboard shot down, escapes with invisibility.
 - Werejackal: like tigers, bears, jackals, brought to fruition by Leonin.
 - Jackal evades Giacomo and Roland in engine room, Giacomo held gunpoint
 - Jackal gets away, finds Cassidy and Arthur hopping off the cruiser
 - Jackal chases, all party free falls, almost kills Jackal
 - Jackal escapes with a winged contraption, we feather fall land on Loosey Goosey
### Loosey Goosey
 - Whole yacht is an orgy machine. 
 - Roland joins the orgy on Loosey Goosey temporarily, gets more drugs
 - Jack and Giacomo break into captain's quarters, take yacht to Prince yacht
 - Prince Desmond gives free passes rest of the month, thanks for saving his life
 - Roland agrees to split whale money, joins in orgy after taking full drugs


# You Spin Me Right Round Baby [6/17/2021]
### Yup, We're Doomed
 - Giant worm burst through a building, we held up in the Iron Wolves Guild.
 - Defeated thanks to some bombing squads. 
 - Found out that Lucian has vaccines that can stop the activation
 - Activation was for the homeless only

### The Twilight Gap
 - Totally cut off from rest of the city
 - hiding place for a druid during war, completely overgrown with vines
 - Decided to take the police skiff over to the gap.
 - Went in through the tunnel covered in moss
 - Use block and tackle to climb down a waterfall from tunnel into city
 - Jump and climb between vines on the skyscrapers to make it towards the
 high growth area

### Arthur is a Lich Confirmed
 - find a skyship hanger in center of city with a whirring gear
 - find an incinerator up and running
 - Disable incinerator using a small clockwork hacking device
 - Find a druid in a phlogiston ingestion chamber, room is sabotaged to explode
 - Party gets through iron latch into the boiler room with spinning gears,
 - Party finds robed druid, give chase across spinning gears, struggle w/ console
 - Jack jumps across (w/ Giacomo), but fails, almost falling into the lava, saved by Gizmo
 - Jack double nat 20s, makes it across with help of everyone, Giacomo nat 20
 immediately disables and releases water to the incinerator
 
### Cooling System
 - Find a storage room w/ stuff and red magiquartz and a map of the area.
 - Find a final construction ship room and water pressure release room
 - Overheard that the lich was here.
 - History: Place in Drovania that had all clockwork soldiers turn red and go hostile
 - Find ritual circles for summoning a Sabbathune, find dimensional portal circles.
 - Giacomo gives a quick lecture after seeing the circles, why they seem implausible.
 
### "Mistakes were made..."
 - Dregzund: "How could you do this? Wasn't meant to happen so quickly. Why?"
 - Lucian: "I'm sorry. I don't have the patient. We can draw the blue lady out, and
 I can get my revenge. I know this isn't going according to plan but I can't follow
 your orders anymore."
 - "Orders? You are messing with things you don't understand. I am far older,
 and wiser, and have the patience you cannot imagine."
 - "I may have a long lifespan, but I'm not waiting for something I don't 
 understand."
 - miss a section...
 - "Where are your other druids? They should be back. Heard something crash."
 "Take care of it. I'm sure the police have dealt with your experiments. We
 have been set back by your ignorance."
 - "Mistakes were made. I'll make it up to you."
 - We entered the underground hanger bay towards the flying ship.
 - The ship is a black magiquartz yacht
 
### The ~~Double~~ ~~Triple~~ Quadruple? Cross
 - The Yacht is Dregzund's Phylactery 
 - Lucian catches Jack, Arthur steps up, makes a false story about being
 a runaway boy. 
 - Gentleman Jack makes an argument to Lucian, that attacking the slums is not
 the right decision. That the Blue Lady must be brought down from within.
 - Huge chunk of Lucian has been blown out, held together by vines.
 - Jack promises to work as an informant if we can get some vaccines back
 that we will work through the Blue Lady's system to find the enforcers.
 - Lucian: Jack must get a position in the Blue Lady's higher court. And in
 exchange, Jack will give intel about the Blue Lady's higher-ups.
 - Archdruid gave Jack a Mark of Lucian (Mark of Handling)
 - Lucian: "What can i call you?" "The Shepherds [of Humanity]"
 - Giacomo gets launched into the ocean. 

# Crawling in my Skin [6/10/2021]

#### Iron Wolves meet-up
 - K8 Sig is forced to clean all of lower district w/ clockworks
 - While we are in no legal trouble, we are banned from work for a month
 - Ashton went to meet up with Bloodhunters (gone this session)
#### Week of down time
 - Arthur gets job as server, makes 9 gold
 - Evan gets turned down by a job
 - Clinc loses 10g gambling
 - Jack gets a license at the DMV, contact from Solatium (+8g)
 - Solatium contacts Jack about following up on poisoned water supply
 - Meeting "Rowanna" at Eer'bys 
 - Giacomo tries to find a vendor to sell magiquartz, but with no luck.
#### Wastewater Treatment Plant
 - Rowanna / Jack pick the lock
 - Clinc / Giacomo find small black worms in the water supply, necromancy
 - Water supply is dusty, does not look cleaned, no security.
 - Rowanna doesn't know supply lines, struggles with father
 - "Bill Dingle" father of Rowanna
 - Address of warehouse: Lower District apt 4, complex 3, building 7
#### The Shady Individual
 - Found man in green robe, ran for it. 
 - Gave chase, ran into room with tube that started to fill w/ water
 - Dock 13, Water Treatment Facility
 - Ran into 3 more druids, one escaped. The other two were captured.
 - One announced that 'Lucian will claim victory'
 - Lucian Veil: Human druid, trained with elves, conscripted into war (or dodged)
#### Rowanna is Shady AF
 - Headed to warehouse, passing some rude ass homeless
 - Giacomo and Clinc suspicious of Rowanna b/c water breathing, shady behavior
 - Jack promises to tail her
 - Fine-Dressed Man Mr. Benjamin (Mickey) appears - claimed to run company
#### Interrogation (offscreen) - "Avery" the druid talked
 - Two druids killed, one poison, one slashed.
 - Lucian Veil working with Lich to poison water supply. Parasites in water.
 - Lich wants to control Lucian; Lucian wants to kill machine city; allies fighting.
 - Water Supply is still active, plenty of agents down below.
 - Lucian stepped in fey-power, gained archdruidic power. 
 - Blue Lady made druids fight against elves, caught in middle. Saw Lucian go crazy.
#### These Wounds they will NOT HEAL
 - Jack comes to apartment, tells us (sort of) what happened
 - Rowanna reveals herself as Cassidy. 
 - Lesser restoration expels the parasites directly, traumatizing.
 - Giacomo collects another sample from blood
 - Giacomo does research, Clinc/Evan try medicinal solutions, Jack searches for answers
#### We don't go back to Ravenholm
 - Went to Icora to present the evidence of samples, showed Jack being cured.
 - As that occurred, the whole city burst out into tentacle-like worm creatures
 - We hit level 4.

# Arrested Development [6/3/2021]

Quick Recap
 - Week of down time. Clinc & Giacomo went rummaging in trash. Jack tried to stake out the water supply. Giacomo struggled to sleep due to a local family with loud crying children.
 - Arthur & his uncle went and got real jobs to earn a wage, Ashton and them stayed at local pubs.
 - Jack told us that a Lich had been poisoning the local water supply. Giacomo made a tub of water for Clinc in the bachelor pad.
 - Got intel about an undead cult kidnapping "ladies of the night."
 - Ashton went to a Lower district whorehouse (Vinichi City) to try to find Amy, who had info on the cult. He was turned away.
 - Jack and Giacomo went to try. Jack cast Ashton as a stalker in order to seem like an authority. 
 - Jack spoke with Amy. Found out there have been cover-ups for kidnappings by masked individuals (dragon head). Sarah (brunette hair, standout eyes, petite), a lady of the night, had been captured. Amy followed, but with no success, dead end.
 - Met VK86, a warforged Iron Wolves member, at a ramen shop. He had intel about the changeling: her name was "Cassidy" and was running with the Black Drask gang. Local gang had been small-time, but now is stealing large amounts of magiquartz from both the Blue Lady and Quazi Quartz industries in the shipping district. Their old haunt was by the City Batteries. 
 - Ashton found a contact about a wanted man, Gunther. Used to run with the Selacium Crime group, but broke off and has been causing issues. Only description: incredibly ugly.
 - Jack, Arthur, and Evan went to investigate the old haunt. They found some gang members watching the streets, but nothing high profile.
 - Giacomo, Clinc, and Ashton went to the Shipping District to investigate the missing magiquartz. Heard news that Harbormaster Foxin had gone missing (for weeks). 
 - Another harbormaster, Vildehamin Nojur (Quazi Quartz), was receiving a shipment that day. Giacomo spoke to one of his employees and discovered that Dock 21 was the incoming port on the ship Dyla's Wish. Giacomo called Ashton & co. to head to the shipping docks.
 - Giacomo, Gizmo, & Clinc helped transport the payloads off the ship. Right as the last crate was unloaded and the rest of Black Powder arrived, members of the crew (including the foreman Giacomo spoke to) unleashed rounds upon the crew. The foreman revealed herself as a changeling, and the party was locked in a close quarters gunfight.
 - Giacomo killed his first human in self defense with a single blow to the head.
 - Clinc was nearly pushed off a ledge, but he and Gizmo managed to stave off further attacks.
 - Arthur, Jack, and Evan had their elevated exploded by a grenade, but managed to survive. Arthur and Evan focused on take-downs, and Jack went to stop the changeling (after taking a pistol).
 - The changeling transformed into Jack, they tumbled, and (according to Jack's account), the changeling bested him and escaped through the elevator's broke hole before Evan and Arthur could stop them. Evan gave chase, including a gunshot to the shoulder, but couldn't deal with Cassidy's unexpected paratrooper suit after they jumped off.
 - Ashton hunted down Gunther during the fight and honorably 1v1'd him. When Gunther tried to escape from the ledge, Ashton pursued him (using his feather fall token as well), and landed on his corpse in the middle of the streets below.
 - Arthur fell as well, pursuing a couple of Black Drask gang members, but landed safely without issue.
 - Evan, after failing to chase down Jack, was caught red-handed fire shots in the air. He was arrested and taken to District 6 prison.
 - Ashton was arrested for "stalking prostitutes" and for the murder of Gunther. He verbally resisted arrest, but ended up in the District 4 prison. 
 - Giacomo & Clinc gave their full accounts to the police along with a captured robber. When they returned home, they found a badly wounded Jack, robbed of his posessions, except for his clothes. He didn't want to talk about it.
 - Giacomo received a call from Ashton to pay his bail (with the money from the contract). Ashton gave the police substantial difficulty and ensured all his belongings were rightfully returned. Giacomo gave the police an extra bribe as well.
 - Arthur received a call from Evan to pay his bail (which he failed to do, but recovered it the next day by going to another district).
 - Ashton and Jack had a brief stand-off regarding the police troubles, but came to neutral terms. 
 - The Iron Wolves agreed to deal with the legal troubles as part of the investigation. 

# The Lying, the Lich, and the Store Closed [5/27/2021]

Quick Recap
 - Got info from Guild Mage about Sabbathune - weak to fire, lurks at night
 - Landlord informed about inevitable foreclosing on Como & Clinc's Clocks, 500g
 - Ventured into the feyline woods towards evening
 - Ran into trouble with zombified plants (necromancy/druidic origin)
 - Found a dark cave where Sabbathune slept, investigated thoroughly
 - Fought and defeated the Sabbathune, took its hand as a trophy for the guild
 - Followed its masters, found a druid grove with a necromancer cabal.
 - Cabal absolutely dunked us, but ressurected Clinc and gave us a chance to escape (thanks to Jack)
 - Ran home ~2am arrival, Arthur insisted on going to Iron Wolves, Jack met with contacts
 - Iron Wolves took it off our plate, gave us 250g payout
 - I created Gizmo, my first finally working prototypical Clockwork Man!
 - Clinc and I sold everything we could, but could not pay the 500g. The store was sold off.
 - I bought a pistol.
