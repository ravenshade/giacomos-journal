# Engineering & Business Ledger

## Services

| Service     | Cost | Info |
| ---      | ---      | ---      |
| Orinshire Tram Ride  | **8s** |   |
| Orinshire Payphone   | **8c** | Cost may depend on duration |
| Orinshire Rent (Poor)|**10s**/wk | Cost depends upon location |
| Orinshire Food (Avg) | **3s**/wk | | 
| Airship Cross Continent | **50g/p** | |
| Airship Across Ocean | **100g/p** | |
| Airship City-to-City | **25g/p** | |
| Teleportation Circle | **250g/p** | |
| Luber Transportation | 10 lb magiquartz / hour | |

## Items

| Item     | Cost | Info |
| ---      | ---      | ---      |
| Magiquartz Pistol | **100g**   | 2d6 force, 40/120, light   |
| Magiquartz Shotgun | **150g**  | 3d4 force, 15ft cone |
| Magiquartz Rifle | **250g**   |  2d8 force, 100/300, two-handed  |
| Magiquartz Sniper | **500g**   |  2d12 force, 200/400, two-handed  |
| Glove of Blasting | **100g**   |  Cast Scorching Ray  |
| 20x Pistol/Rifle Ammo | **30g**   |    |
| 1 lb Magiquartz (white)  | **30g**   |  Crafting/Energy  |
| Grappling Hook Gun "Gap Closer" | **25g** | 50ft grappling both ways |
| Grappling Arm | **25g** | Shoots hook 60ft can retract |
| Shield Ring | **100g** | 3 charges. Cast shield spell. Roll 1d4-1 dawn. Attunement. | 
| Flashlight | **50g** | 60ft bright, 60ft dim, 120ft cone. Infinite time. |
| Feather Token | **10g** | Actives Feather Fall after 40ft fall |
| Shielding Bracelet | **50g** | reduce damage 1d4 |
| Spell Shard | ?? | Holds 320 pages of knowledge |
| Skyboard | **250g** | Attunement. Flight 80ft per round. Costs 1lb magiquartz per hour |
| 5x Smoke Bombs | **10g** | 10x10ft cube smoke | 
| Explosive Grenade  | **25g** | DC14 3d6 fire (half save) | 
| 2x Comm Device | **50g** | Can talk to other side within 150ft |
| Shimmerweave   | **50g** | Can transform into different outfits| 
| Motor Bike     | **150g** | Motor bike, 1 lb magiquartz / hour | 


## Vehicles 

| Vehicle  | Cost | Info |
| ---      | ---      | ---      |
| Car      | **1000g**   |   |
| Skysail Ship | **15,000g** | Small. 5 crew / 15 passenger |
| Weatherlight Cruiser | ??? | Medium. 5 crew / 35 passengers. 5 ton cargo |
| Powerstone Cruiser | ??? | Yacht. ??? |
