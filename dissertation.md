[[_TOC_]]

# Ex Peculiari Doctrina de Automata Cognitione Reflexionem Emergens - Giacomo Vinci

**Translation**: Special Theory of Automata Emergent Cognition

The dissertation is a refutation of the Grand Unified Theory of Mana based on 
warforged's sudden emergence of consciousness. 

Gist: Time and consciousness are an omnipresent axis and mana is the physical representation
of temporal cognition (similar to how acceleration is the manifestation of spacetime).

Experiment: Gizmo -> Warforged v. true warforged

# Historical Overview of Natural Philosophy

## Merlin's Rules

The foundation of magical study. A set of principles, rules, and esoteric
moral practices surrounding spellcasting and the control of mana. 

Widely considered to be outdated, but several principles stand despite attempts
to disprove their ramifications.

Merlin purported three universal axioms:
 1. Mana determines the nature of the physical realm.
 2. Magical ability comes from manipulating mana. 
 3. Mana cannot be manipulated without conscious intent.

### Ramifications (and other assertions)

 - Spellcasting requires a soul, which acts as a bridge outside of the physical realm to access mana.
 - Gods are simply powerful spellcasters, which is why their powers are not omnipotent, and chaos v. order
 (good v. evil) is a conversation about proper mana distribution. 
 - There is a realm outside of the physical realms which governs magic's implementation.
 - Magic spellcasting can be learned by any student presuming they possess consciousness. 
 - Words and language have innate magical ability, for thought and intent is required to cast spells.
 - Magic cannot be applied without a caster.
 - There is a moral determination to whether magic will be applied to the physical realm due to its
 connection to the soul.
 - It is possible that mana exists in infinite quantities, and that a different system of nature governs.
 - There may be no limit to the amount of manipulation mana can have on our physical realm. 
 - Necromancy is the most forbidden of all magic because it manipulates the bridge between the physical
 and realms of mana, and in some cases, permanently destroys it.

## Aumvor's Sorcerer Experiments - ???

TL;DR Performed by a necromancer and digusting, generally discouraged. Demonstrated that sorcerers
have a finite amount of power within them that they draw from in order to perform magic.

The experiments involved brutal mutiliation of subjects, soul magic, and precise measurements of
mana capacity. 

Generally disregarded for their lack of academic rigor. 

## Alchemical Law of Equivalent Exchange - ???

Experimental proof that, given a set of chemical reactions, natural philsophers can predict
precisely what the output will be given base elements. 

Elements react together naturally. There is always an equivalent exchange between the inputs
and outputs of any given exchange. 

Mana and magical can manipulate the exchange to distort the ratios, but there is always a
physical balance of substance without magic. 


## ???'s Ward Experiments - ???

TL;DR it seems wards govern conscious intent rather than explicit instructions.


## Theory of Mana as Manifested Ego - ???

TL;DR If mana is external to physical realm, then it is the puppetmaster for free will.
Instead of consciousness controlling mana, mana is choice in the physical realm. All beings
are controlled by a well of mana known as the soul, and the physical realm is therefore determined
by the largest concentration of will (mana) in a given area. 

This theory was never popularized because of its association with druidism that had been
discouraged during the war. 


## Grand Unified Theory of Mana - ???

Critical Theory that mana is a finite, measurable, predictive physical excitation of matter.

When matter is in a state of mana excitation, its energy, mass, position, temporal status, 
structural integrity, and elemental nature are unstable. 

Interaction between two particles with different mana excitations leads to different
outcomes of properties exchange. 

Spellcasting efforts are all directed at controlling the level and interaction of mana excitement.

 - Ritual circles allow natural collection of excitation
 - Spell circles and verbal components follow properties of the Law of Equivalent Exchange,
 but with a resource distortion proportional to the amount of mana excitation in the equation. 
 - Mana excitation is conserved during all transformations, which can be witnessed through the
 formation and transferrence in ley lines and magiquartz. 

### Experimental Demonstrations

 - Black and White Magiquartz act as imperfect vessels of mana excitation but transfer excitation similar
 to leylines. Experimentally, white magiquartz can be drained into black magiquartz, but no excitation is lost within
 these closed systems. 
 - Alchemical experiments that previously were exceptional in the Law of Equivalent Exchange can be explained by
 a mana transferrence into or out of the system. 

### Ramifications

Grand Unified Theory of Mana conflicts with Merlin's first and third axioms. 
 - Mana is part of the physical realm as a property of matter. 
 - The mana level of a magiquartz crystal can be measured, as can its ability to transfer to
 other matter. 
 - Magic manipulation of matter does not require concious intent and can occur whether the caster
 intended its effects or not.
 - There is a physical limitation to the power of magic in manipulating nature. 
 - The mana derived from 'souls', blood, and sacrifice merely comes from the mana excitations 
 present in the matter of biological beings.

### Unified Papers
 - On the Law of Distribution of Mana in the Physical Spectrum - Planck
 - On the Constitution of Mana and Matter - Niels Bohr
 - Mana Emission and Absorption according to Law of Equivalent Exchange - J. Stark
 - On the Influence of Magnetism on the Nature of Mana Emitted by Matter - Zeeman
 - On the Continuity of the Gas and Liquids during Mana Excitation - Van der Waals
 - The High Frequency Spectra of Alchemical Transfusions - H. G. Moseley
 - Ionization of Magiquartz in electric fields - Arnold Sommerfeld
 - On the Application to Mana of a General Mathematical Method - William Rowan Hamilton
 - On Conservation of Conditionally Periodic Mana Excitations for Change in Hamilton's Function - Kolmogorov
 - On the Superconductivity of Mana at extreme temperatures - Kamerlingh Onnes
 - Quantization of Matter Transfusion as an Eigenvalue Problem - E. Schroedinger

## ???'s Documents on the Creation of Warforged

TL;DR talks about how warforged were built and the unexpected findings therewithin.
