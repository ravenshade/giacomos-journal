# Suspects & People

## Black Powder | The Shepherds [Party]

| Name           | Race        | Class       | Spec          | Player  |
|----------------|-------------|-------------|---------------|---------|
| Giacomo Vinci  | Vuman       | Artificer   | Battle Smith  | Kenny   |
| Clinc          | Warforged   | Monk        | Way of Mercy  | Will    |
| Gentleman Jack | Vuman       | Rogue       | Swashbuckler  | Edward  |
| Evan Alderman  | Vuman       | Fighter     | Battle Master | Morgan  |
| Mer            | ???         | Druid       | Land??        | Taylor  |

### Former Members
| Name           | Race        | Class       | Spec          | Player  | Status | Why |
|----------------|-------------|-------------|---------------|---------|--------|-----|
| Cadence        | High Elf    | Bard        | N/A           | Anne    | Dead   | Killed during one-shot mission (Anne left) |
| Ashton McGuire | Fire Genasi | Bloodhunter | Ghostslayer   | Michael | Hiatus | Temporary player hiatus, with bloodhunters |
| Arthur Helby   | Vuman       | Bard        | Creation      | Taylor  | Left   | Due to "professional differences" |


## Iron Wolves [Guild]
 - Icora (Guild Contact): Strict lady. We best keep her happy.
 - Guild Mage Felwinter: Hates casting magic, helped us with the Sabbathune.

## Lich Dregzund Imitez [Druid/Necromancer Cabal]

All powerful necromancer and lich working with the druid rebellion that once attacked the blue city.

Supposedly trains "Dark Apprentices" in Tenebrius Tower.

## Black Drask Gang
 - Gunther (deceased): Former member of the Selacium Crime gang, but broke off. Killed at Dock 21 by Ashton.
 - "Cassidy" Changeling: Currently escaped after Dock 21 incident.

# Missions

## Find Changeling causing trouble in the city

Changeling "Cassidy" is running with the Black Drask Gang.

Ran into the gang stealing magiquartz, but Cassidy managed to kidnap Jack and escape. The rest were taken down, including "Gunther", a wanted former member of the Selacium crime syndicate.

## Defeat Creature in forest hindering lumberjacks (Complete)

The lumberjacks ran into issues dealing with the Feyline woods.

We found a great Sabbathune and put it down, but when we followed its masters, 
we ran into a terrifying necromancer cabal in a druid grove.

Luckily, Jack was able to talk the escalation down, and we escaped with minor
wounds, thankfully.

**Payment:** 250g
