# Travel Ledger

Contains information regarding places we have traveled or heard rumors of.

[[_TOC_]]

# Orinshire

Major metropolitan area. 

 - Iron Wolves Guild
 - Como and Clinc's Clocks (Closed)

## Shipping District

Frequent airships and drop-offs. Run by harbormasters, port authorities. 

 - Vildehamin Nojur: Quazi Quartz Contact

### Weaver's Guild HQ

Weaver's Guild control the import and export around the city. 

## Lower District

### Bachelor Pad (Jack)

Shitty resident with a bathroom, mattresses, not much else. Public phone available.

### Vinichi City Whorehouse

Thinks Ashton is a stalker.

 - Amy: Lady of the Night. Contact about Undead Cult.
 - Sarah (missing): Previous lady of the night. Taken by cult.

### Water Supply

Jack has been staking out this place. Known to be poisoned by the lich.

## Twilight Gap

Part of the city that has been completely taken over by trees, jungle vines, and other detrius.

Known for its dangerous animals and for once being a druid hideout.

## Feyline Woods

North of Orinshire city. Known locale of lumberjacks cutting down the woods. 

Previously the location where druids staged an onslaught assault on the city during the War.

# Tenebrius Tower

The famed home of Lich Dregzen Imitas.

# Sinright City

### Secret Underground Tram
