![Giacomos Journal](/images/giacomo-journal-header.png "Giacomos Journal")

# Entries

If lost, please return to **Giacomo Vinci**. 

 - [Party Inventory](inventory.md)
 - [Suspects, Missions, & People](people.md)
 - [Travel Ledger](travel.md)
 - [Engineering & Business Ledger](business.md)

# Logs

Various writings on our adventures

 - [Find Yourself in Yourself](logs.md)
 - [Black Mesa: On a Rail](logs.md)
 - [Take me down Sinright City](logs.md)
 - [Blue in the Face](logs.md)
 - [Panic! At the Disco](logs.md)
 - [You Spin Me Right Round Baby](logs.md)
 - [Crawling in my Skin](logs.md)
 - [Arrested Development](logs.md)
 - [The Lying, the Lich, and the Store Closed](logs.md)

# Other
 - Cassidy Nat 1s: 8
